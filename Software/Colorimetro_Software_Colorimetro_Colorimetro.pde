/* Copyright 2015 Pablo Cremades
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 05/08/2018
 * e-mail: pablocremades@gmail.com
 * Descripción: Esta aplicación es una interface alternativa para el colorímetro de IORodeo (https://iorodeo.com/products/educational-colorimeter-kit).
 *
 * Change Log:
 */
import g4p_controls.*;
import grafica.*;
import processing.serial.*;


GPlot RGBplot;
GPointsArray sensorData = new GPointsArray( 4 );
Serial port;
GButton Medir;
GButton Calibrate;

void setup(){
 size( 800, 600 );
 if( openComm() == 1 ){
   println( "No hay ningún colorímetro conectado" );
   exit();
 }
 
 //sensorData = new GPointsArray( new float[] {0,0,0} );
 RGBplot = new GPlot( this, 10, 10, 400, 400 );
 RGBplot.setYLim(0, 2);
 RGBplot.startHistograms(GPlot.VERTICAL);
 RGBplot.getHistogram().setDrawLabels(true);
 //RGBplot.getHistogram().setRotateLabels(true);
 RGBplot.getYAxis().getAxisLabel().setTextAlignment(RIGHT);
 RGBplot.getHistogram().setBgColors(new color[] {
    color(255, 0, 0), color(0, 255, 0), 
    color(0, 0, 255), color(200, 200, 200)} );
 
 Medir = new GButton(this, width/2 + 100, 20, 60, 40, "Medir");
 Calibrate = new GButton(this, width/2 + 200, 20, 90, 40, "Calibrar");
 sensorData.add(0,0,"R");
 sensorData.add(1,0,"G");
 sensorData.add(2,0,"B");
 sensorData.add(3,0,"W");
}

void draw(){
  background(210);
   //RGBplot.defaultDraw();
  RGBplot.beginDraw();
  RGBplot.drawBackground();
  RGBplot.drawBox();
  RGBplot.drawYAxis();
  RGBplot.drawTitle();
  RGBplot.drawGridLines(GPlot.BOTH);
  RGBplot.drawHistograms();
  float absRound = round(1000*sensorData.getY(0))/1000.0;
  RGBplot.drawAnnotation( str(absRound), 0,sensorData.getY(0) + 0.1, 1, 1);
  absRound = round(1000*sensorData.getY(1))/1000.0;
  RGBplot.drawAnnotation( str(absRound), 1,sensorData.getY(1) + 0.1, 1, 1);
  absRound = round(1000*sensorData.getY(2))/1000.0;
  RGBplot.drawAnnotation( str(absRound), 2,sensorData.getY(2) + 0.1, 1, 1);
  absRound = round(1000*sensorData.getY(3))/1000.0;
  RGBplot.drawAnnotation( str(absRound), 3,sensorData.getY(3) + 0.1, 1, 1);
  RGBplot.endDraw();
}

void serialEvent(Serial port) { 
  String inString = port.readString();
  //Buscamos un string válido.
  if ( inString.length() > 30 && inString.charAt(0) == '[' ) {
    String[] list = split(inString, ","); //Split the string.
    float[] freq = {float( list[1] ), float( list[2] ), float( list[3] ), float( list[4] )};
    float[] trans = {float( list[5] ), float( list[6] ), float( list[7] ), float( list[8] )};
    float[] abs = {float( list[9] ), float( list[10] ), float( list[11] ), float( list[12].substring(0, list[12].length()-3) ) };
    sensorData.set(0, 0, abs[0], "R");
    sensorData.set(1, 1, abs[1], "G");
    sensorData.set(2, 2, abs[2], "B");
    sensorData.set(3, 3, abs[3], "W");
    println(list[12].substring(0, list[12].length()-3));
    RGBplot.setPoints(sensorData);
  }
  inString ="";
}

//Manejador de eventos de los Botones "Reiniciar" y "Ajuste".
public void handleButtonEvents(GButton button, GEvent event) {
  if ( button == Medir ) {
    port.write("[1]");
    delay(100);
  }
  if ( button == Calibrate ){
    /*port.write("[5]");
    delay(100);
    port.write("[6]");
    delay(100);
    port.write("[7]");
    delay(100);
    port.write("[8]");
    delay(100);*/
    port.write("[0]");
    delay(100);
  }
}

int openComm() {
  String[] serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB0") || serialPorts[i].contains("COM") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 9600);
        port.bufferUntil(10);
      }
      catch(Exception e) {
        return 1;
      }
    }
  }
  if (port != null)
    return 0;
  else
    return 1;
}
