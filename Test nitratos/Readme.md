# Test de nitratos

Realizamos dos pruebas con el kit de [Hach NitraVer5](https://www.hach.com/nitraver-5-nitrate-reagent-powder-pillows-10-ml-pk-100/product?id=7640208933) para la determinación de nitratos utilizando el colorimetro con una luz aproximadamente UV (400nm).

Ambos test fueron realizados en los laboratorios de la FCEN - UN Cuyo por Pablo Cremades, Nano Castro y Florencia Ruggeri.

### Primer test

Utilizamos una solución patrón de 100 ppm de Nitrato de Potasio y agua destilada para obtener diluciones de concentraciones crecientes: 0, 20, 37.5 y 60 ppm.  

<img src="/Test nitratos/IMG_20180822_131347.jpg" width="400"/> <img src="/Test nitratos/IMG_20180822_131359.jpg" width="400"/>

El resultado obtenido fue una lineal con un ajuste de R<sup>2</sup>=0.9848.  

![primer test](/Test nitratos/primer test.jpg)



### Segundo Test

Realizamos un segundo test con una mayor cantidad de diluciones de la solución patrón de Nitrato de Potasio (100ppm) para obtener más puntos en la calibración: 0, 10, 20, 30, 40, 50, 60, 70 y 80 ppm.  

Luego contrastamos los resultados obtenidos con el colorímetro con los de un equipo comercial Shimadzu perteneciente a la FCEN.  

<img src="/Test nitratos/20190517_122540.jpg" width="400"/> <img src="/Test nitratos/20190517_122600.jpg" width="400"/>

<img src="/Test nitratos/20190517_122458.jpg" width="400"/> <img src="/Test nitratos/20190517_122522.jpg" width="400"/>

El resultado obtenido fue lineal tanto para nuestro colorímetro como para el equipo comercial con un R<sup>2</sup> de ajuste mayour a 0.9.  

![segundo test](/Test nitratos/segundo test.jpg)

Aun falta trabajar en nuevos test para determinar la desviación de las mediciones y evaluar la respuesta del método con muestras de agua .






